﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace TextAdventure
{
    public class Game
    {
        private String gameName = "Riddles";   // a String initialized to empty
        private bool shouldExit = false; // Create a bool (true/false logical operator) indicating if we should exit the game
        private int question = 0;
        String cm = "";

        // Constructor for the Game class
        // This is the code that is called whenever an instance of Game is created
        public Game()
        {

        }

        // Function (segment of logic) called from Program.cs when the application starts
        public void Setup()
        {
            // Storing the String "My First Game" (without quotes) in the variable named "gameName"
            gameName = gameName + " ..What am I?";
        }

        public void Begin()
        {
            // Print various lines of strings to the Console window
            Console.WriteLine("************************************");
            Console.WriteLine("Welcome to " + gameName);    // the + operator concatenate two strings (combines them)

            // Loop until shouldExit is true
            while (shouldExit == false)
            {
                HandleCommand(cm);
            }
        }

        public void HandleCommand(String command)
        {
            command = command.ToLower();
            while (question == 0)
            {
                Console.WriteLine("What is in seasons, seconds, centuries and minutes but not in decades, years or days?");
                command = Console.ReadLine();
                if (command == "n") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 1)
            {
                Console.WriteLine("The person who makes it, sells it. The person who buys it never uses it and the person who uses it doesn't know they are. What is it?");
                command = Console.ReadLine();
                if (command == "a coffin") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 2)
            {
                Console.WriteLine("What goes up but never comes down?");
                command = Console.ReadLine();
                if (command == "age") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 3)
            {
                Console.WriteLine("I sit in a corner and travel around the world.");
                command = Console.ReadLine();
                if (command == "a stamp") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 4)
            {
                Console.WriteLine("Lighter than what I am made of, more of me is hidden than is seen.");
                command = Console.ReadLine();
                if (command == "an iceberg") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 5)
            {
                Console.WriteLine("You heard me before, yet you hear me again, then i die, 'till you call me again.");
                command = Console.ReadLine();
                if (command == "an echo") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 6)
            {
                Console.WriteLine("Two in a corner,  1 in a room, 0 in a house , but 1 in a shelter.");
                command = Console.ReadLine();
                if (command == "r") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 7)
            {
                Console.WriteLine("It cannot be seen, it weighs nothing, but when put into a barrel, it makes it lighter.");
                command = Console.ReadLine();
                if (command == "a hole") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 8)
            {
                Console.WriteLine("What starts with a T, ends with a T, and has T in it?");
                command = Console.ReadLine();
                if (command == "a teapot") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 9)
            {
                Console.WriteLine("What gets wetter as it dries?");
                command = Console.ReadLine();
                if (command == "a towel") question++;
                else Console.WriteLine("Try Again");
            }
            while (question == 10)
            {
                Console.WriteLine("What question can you never answer honestly to?");
                command = Console.ReadLine();
                if (command == "are you dead?") question++;
                else Console.WriteLine("Try Again");
            }

            while (question == 11)
            {
                Console.WriteLine("Congratulations!");
                question++;
            }
           
        }
    }
}


            /*Console.WriteLine("What is in seasons, seconds, centuries and minutes but not in decades, years or days?");
            String command = Console.ReadLine();
            command = command.ToLower();
            
            if (command == "n")
            {
                Console.WriteLine("The person who makes it, sells it. The person who buys it never uses it and the person who uses it doesn't know they are. What is it?");
                command = Console.ReadLine();
                if (command == "a coffin")
                {
                    Console.WriteLine("What goes up but never comes down?");
                    if (command == "age")
                    {
                        Console.WriteLine("What lives in the corner but travels the world?");
                    }
                } 
            }
         
  else
            {
                Console.WriteLine("Try again");
            }


        }
    }
}*/
